#define _CRT_SECURE_NO_WARNINGS
#pragma once 
#include"Date.h"


//d1 == d2 
bool Date::operator == (const Date& d)
{
	return _year == d._year &&
		_month == d._month &&
		_day == d._day;
}

//d1!=d2
bool  Date::operator != (const Date& d)
{
	return !(*this == d);
}


//d1 > d2
bool  Date::operator > (const Date& d)
{
	if (_year > d._year)
	{
		return true;
	}
	else if (_year == d._year && _month > d._month)
	{
		return true;
	}

	else if (_year == d._year && _month == d._month && _day > d._day)
	{
		return true;
	}

	return false;
}

// d1 >= d2
bool Date:: operator >= (const Date& d)
{
	return *this > d || *this == d;
}

bool Date:: operator <= (const Date& d)
{
	return  !(*this > d) || *this == d;
}

//天满了进月，月满了进年，年无穷无尽
Date& Date:: operator += (int day)
{
	//应对加负数的情况
	if (day < 0)
	{
		return *this -= abs(day);
	}



	_day += day;

	while (_day > Getmonthday(_year, _month))
	{
		_day -= Getmonthday(_year, _month);
		_month++;
		if (_month == 13)
		{
			_month -= 12;
			_year++;
		}
	}
	return *this;
}


Date  Date::operator +(int day)
{
	Date ret(*this);//拷贝构造
	ret += day;
	return ret;//出了作用域已经不在了，不能引用
}

Date& Date:: operator -= (int day)
{
	//应对减负数的情况

	if (day < 0)
	{
		return *this += abs(day);
	}


	while (day)
	{
		if (_day == 0)
		{
			_month--;
			_day = Getmonthday(_year, _month);
			if (_month == 0)
			{
				_year--;
				_month = 12;
			}
		}
		_day--;
		day--;
	}
	return *this;
}

Date  Date::operator -(int day)
{
	Date ret(*this);
	ret -= day;

	return ret;
}




Date& Date::operator = (const Date& d)
{
	if (this == &d)
	{
		return *this;
	}
	_year = d._year;
	_month = d._month;
	_day = d._day;

	return *this;
}

//前置后置的区别无非就是返回值的不同

//前置
Date& Date::operator ++ ()
{
	*this += 1;
	return *this;
}

//后置
Date Date::operator ++(int)
{
	Date ret(*this);//要记得构造拷贝函数哟！
	*this += 1;
	return ret;

}


Date& Date::operator --()
{
	*this -= 1;
	return *this;

}
Date Date::operator --(int)
{
	Date ret(*this);
	*this -= 1;
	return ret;
}
