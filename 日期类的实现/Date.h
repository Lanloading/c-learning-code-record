#define _CRT_SECURE_NO_WARNINGS
#include <iostream>
using namespace std;

class Date
{
public:

	int Getmonthday(int year, int month)
	{
		static int monthday[13] = { 0,31,28,31,30,31,30,31,31,30,31,30,31 };
		if (month == 2 && ((year % 4 == 0 && year != 0) || (year % 400 == 0)))
		{
			return 29;
		}
		else
		{
			return monthday[month];
		}
	}

	Date(int year = 1, int month = 1, int date = 1)
	{
		_year = year;
		_month = month;
		_day = date;
	}

	void Print()

	{
		cout << _year << " " << _month << " " << _day << " " << endl;

	}

	//d1 == d2 
	bool operator == (const Date& d);
	//d1!=d2
	bool operator != (const Date& d);
	//d1 > d2
	bool operator > (const Date& d);
	// d1 >= d2
	bool operator >= (const Date& d);
	bool operator <= (const Date& d);
	//天满了进月，月满了进年，年无穷无尽
	Date& operator += (int day);
	Date operator +(int day);
	Date& operator -= (int day);
	Date operator -(int day);
	Date& operator = (const Date& d);

	Date& operator ++();
	Date operator ++(int );

	Date& operator --();
	Date operator --(int);




private:
	int _year = 1;
	int _month = 1;
	int _day = 1;

};