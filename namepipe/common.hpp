#pragma once
#include <sys/types.h>
#include <sys/stat.h>
#include <iostream>
#include <unistd.h>
#include <fcntl.h>
#include <string.h>
#include <assert.h>
#include <stdio.h>

using namespace std;

// 通用头文件，实现两个函数，一个创建对应管道，另一个关闭对应管道

// 定义一个路径，用于创建管道
#define PIPE_PATH "/home/lanload/pipe/namepipe/mypipe"

// 管道创建文件
bool buildpipe(const string &path)
{
    // 创建管道函数：mkfifo,成功返回 0 失败返回 -1,同正常创建文件相同，创建的时候指定当前文件的权限

    // 创建前，设置一下权限掩码
    umask(0);

    // 创建管道文件
    int ret = mkfifo(path.c_str(), 0600);
    if (ret == 0)
        return true;
    else
    {
        cout << "创建失败!" << endl;
        return false;
    }
}
//销毁管道文件

bool delpipe(const string& path)
{
    int ret = unlink(path.c_str());
    if(ret == 0)    return true;
    else 
    {
        cout<<"del failed!"<<endl;
        return false;
    }
    
}