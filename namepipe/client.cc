#include"common.hpp"

int main()
{

	//客户端，打开对应文件后返回对应的fd用于传输消息
	int wfd = open(PIPE_PATH,O_WRONLY);
    assert(wfd != -1);

	char buffer[1024];
    std:: cout<<"客户端启动！"<<buffer<< std::endl;
	
	while(true)
	{
		//从对应的输入流读取输入的字符
		std::cout<<"请输入：";
		fgets(buffer,sizeof (buffer),stdin);

		if(strlen(buffer) > 0)
		buffer[strlen(buffer)-1] = 0;

		ssize_t ret = write(wfd,buffer,strlen(buffer));
		assert(ret == strlen(buffer));

	}

	//最后关掉对应的fd
	close(wfd);
	return 0;
}