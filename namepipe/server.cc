#include"common.hpp"
//服务器端，用来管理管道文件，以及接受客户端发送的消息


int main()
{
    //那么客户端要做的第一件事情应该就是创建对应的管道。
    bool check = buildpipe(PIPE_PATH);
    assert(check);

    //to do
    //那么客户端应该做的事情应该是不断的检查管道的读端是否有数据,不同于打开文件fd，这次服务端和客户端都应该打开一份相同的文件
    //调用系统接口open来打开文件，返回对应的文件fd
    int rfd = open(PIPE_PATH,O_RDONLY);
    assert(rfd != -1);

    char buffer[1024];
    std:: cout<<"服务器启动！"<<buffer<< std::endl;

    while(true)
    {
        //从对应打开的文件描述符中读取数据存放到buffer中,-1是因为读取为二进制，我们不需要字符串的斜杠零
        ssize_t  ret = read(rfd,buffer,sizeof(buffer)-1);
        if(ret > 0)
        {
            buffer[ret] = 0;
            std:: cout<<"传递的消息："<<buffer<< std::endl;
        }
        else if(ret == 0)
        {
            std:: cout<<"客户端下线了，服务器告辞"<< std::endl;
            break;
        }
        else
        {
            std::cout << "err string: " << strerror(errno) << std::endl;
            break;
        }
    }

    //销毁管道文件
    delpipe(PIPE_PATH);
    return 0;
}
