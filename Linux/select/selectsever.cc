#include<memory>
#include"selectsever.hpp"
#include"log.hpp"

using namespace std;
using namespace Selectns;

int main()
{
	unique_ptr<SelectServer> svr(new SelectServer()) ;

	svr->InitServer();
	svr->start();

}
