#include <iostream>
#include <string>
#include <map>
#include<unordered_map>

using namespace std;

//  先描述，再组织，描述一个聊天室内的一个基本用户面貌,这个头文件只需要管理对应的用户即可。

class User
{
public:
	User(const uint16_t &port, const string &IP)
		: user_port(port), user_IP(IP)
	{
	}
	~User()
	{
	}
	uint16_t user_port;
	string user_IP;
};

// 这个类中，需要管理对应的User结构
class OnlineUser
{
public:
	OnlineUser()
	{
	}

	void add_user(const uint16_t &port, const string &IP)
	{
		// 填充用户的名称，以及对应的端口号，IP

		User user(port, IP);
		string user_name;
		user_name += "[" + IP + "]#:" + to_string(port);
		list.insert(make_pair(user_name, user));
		cout<<"进来一个新用户！当前用户有："<<endl;
		for (auto e : list) std::cout<<e.first<<endl;
		
	}

	void del_user(const uint16_t &port, const string &IP)
	{
		// string user_name = user_name += "[" + IP + "]number:" + to_string(user_num--);
		string user_name;
		user_name += "[" + IP + "]#:" + to_string(port);
		list.erase(user_name);
		cout<<"离开一个用户！当前用户有:"<<endl;
		for (auto e : list) std::cout<<e.first<<endl;

	}

	bool isOnline(const uint16_t &port, const string &IP)
	{
		string user_name;
		user_name += "[" + IP + "]#:" + to_string(port);
		return list.find(user_name) == list.end() ? false : true;
	}

	void boardcast(int socket_fd, string message, const uint16_t &port, const string &IP)
	{

		for (auto e : list)
		{
			struct sockaddr_in client;
			bzero(&client, sizeof(client));

			client.sin_family = AF_INET;
			client.sin_port = htons(e.second.user_port);
			client.sin_addr.s_addr = inet_addr(e.second.user_IP.c_str());

			string s = IP + "-" + to_string(port) + "# ";
			s+=message;

			sendto(socket_fd, s.c_str(), s.size(), 0, (struct sockaddr *)&client, sizeof(client));
		}
	}

	~OnlineUser()
	{
	}

private:
	int user_num = 1;
	unordered_map<string, User> list;
};
