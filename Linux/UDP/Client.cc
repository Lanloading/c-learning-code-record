#include"Client.hpp"
#include "UserManager.hpp"

#include <memory>

using namespace Client;


void Usage(string proc)
{
 	std::cout << "\nUsage:\n\t" << proc << " server_IP server_port\n\n";
}

int main(int argc,char* argv[])
{
	// 输入错误的时候打印手册

	if(argc != 3)
	{
		Usage(argv[0]);
		exit(-1);
	}

	// 获取命令行参数
	string server_IP = argv[1];
	uint16_t server_port = atoi(argv[2]);

	unique_ptr<UDPClient> pClient(new UDPClient(server_IP,server_port));

	pClient ->init();
	pClient ->start();

	return 0;

}