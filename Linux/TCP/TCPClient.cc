#include"TCPClient.hpp"
#include <memory>

using namespace TCPClient;


void Usage(string proc)
{
 	std::cout << "\nUsage:\n\t" << proc << " server_IP server_port\n\n";
}

int main(int argc,char* argv[])
{
	// 输入错误的时候打印手册

	if(argc != 3)
	{
		Usage(argv[0]);
		exit(-1);
	}

	// 获取命令行参数
	string server_IP = argv[1];
	uint16_t server_port = atoi(argv[2]);

	unique_ptr<Client> pClient(new Client(server_IP,server_port));

	pClient ->InitClient();
	pClient ->start();

	return 0;

}