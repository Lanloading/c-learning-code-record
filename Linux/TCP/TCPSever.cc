#include"TCPSever.hpp"
#include<memory>

using  namespace TCPSever;
using namespace std;


void Usage(string proc)
{
	std::cout << "\nUsage:\n\t" << proc << " local_port\n\n";
}

int main(int argc, char *argv[])
{
	// 输入错误的时候打印手册
	if (argc != 2)
	{
		Usage(argv[0]);
		exit(-1);
	}
	// 获取命令行参数
	uint16_t server_port = atoi(argv[1]);
	unique_ptr<Sever> pServer(new Sever(server_port));

	pServer->InitSever();
	pServer->start();

	return 0;
}