

#pragma once
#include<iostream>
#include<functional>
#include <sys/types.h>
#include <unistd.h>

#include "log.hpp"
using namespace std;
//封装一个任务类，让我们传入对应的执行参数，然后执行对应的回调方法即可，而对应的回调方法应该允许自由创建，所以需要封装成
// 一个函数对象

		void ServiceIO(int fd)
		{
			while(true)
			{
				char buffer[1024];
				//此时就相当于文件操作
				int n = read(fd,buffer,sizeof(buffer) -1);
				if(n > 0)
				{
					buffer[n] = 0;
					cout<<"客户端发送的消息:"<<buffer<<endl;

					string s = "服务器回显";
					s += buffer;
					write(fd,s.c_str(),s.size());
				}
				else 
				{
					logMessage(NORMAL,"没客户端了，我也润了");
					break;
				}
			}
		}


class Task
{
public:
	//将一个形参为两个整型，返回值为整型的函数对象用包装器包装起来，称之为fun_t
	using func_t = std::function<void(int)>;
	//无参构造
	Task()
	{}

	//
	Task(int  fd, func_t func )
		:_fd(fd),_callback(func)
	{}

	void operator ()()
	{
        _callback(_fd);
	}
    


private:

    int _fd;
	func_t _callback;

};
