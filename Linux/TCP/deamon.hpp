#pragma once
#include <unistd.h>
#include <signal.h>
#include <cstdlib>
#include <cassert>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

#define DEV "/dev/null" //文件黑洞

// 设置需要被守护进程的目标文件路径

void deamonserver(const char *currPath = nullpt)
{
	// 1.忽略掉部分异常的信号。
	signal(SIGPIPE, SIG_IGN);

	// 2.如何让自己不是组长
	// 既然自己是组长，但是子进程可不是啊
	int n = fork();
	if (n > 0)	exit(0);//父进程退出
    
	pid_t n = setsid();
    assert(n != -1);

	// 3.守护进程是脱离终端的，所以我们需要关闭或者重定向默认打开的文件。
	// 重定向的文件文件描述符为了求问，都重定向到文件黑洞里。

    int fd = open(DEV, O_RDWR);
    if(fd >= 0)
    {
        dup2(fd, 0);
        dup2(fd, 1);
        dup2(fd, 2);

        close(fd);
    }
    else
    {
        close(0);
        close(1);
        close(2);
    }

    // 4. 可选：进程执行路径发生更改

    if(currPath) chdir(currPath);


}
