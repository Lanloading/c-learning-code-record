#pragma once
#include<iostream>
#include<functional>

//封装一个任务类，让我们传入对应的执行参数，然后执行对应的回调方法即可，而对应的回调方法应该允许自由创建，所以需要封装成
// 一个函数对象
class Task
{
public:
	//将一个形参为两个整型，返回值为整型的函数对象用包装器包装起来，称之为fun_t
	using func_t = std::function<int(int,int,char)>;
	//无参构造
	Task()
	{}

	//
	Task(int  x, int y,char op , func_t func )
		:_x(x),_y(y),_op(op),_callback(func)
	{}

	std::string operator ()()
	{
        int result = _callback(_x, _y, _op);
        char buffer[1024];
        snprintf(buffer, sizeof buffer, "%d %c %d = %d", _x, _op, _y, result);
        return buffer;
	}
	


private:

    char _op;
	int _x;
	int _y;
	func_t _callback;

};
int mymath(int x, int y, char op)
{
    int result = 0;
    switch (op)
    {
    case '+':
        result = x + y;
        break;
    case '-':
        result = x - y;
        break;
    case '*':
        result = x * y;
        break;
    case '/':
    {
        if (y == 0)
        {
            std::cerr << "div zero error!" << std::endl;
            result = -1;
        }
        else
            result = x / y;
    }
        break;
    case '%':
    {
        if (y == 0)
        {
            std::cerr << "mod zero error!" << std::endl;
            result = -1;
        }
        else
            result = x % y;
    }
        break;
    default:
        // do nothing
        break;
    }

    return result;
}
