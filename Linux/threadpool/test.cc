#include <iostream>
#include "Thread.hpp"
#include "Threadpool.hpp"
#include "Task.hpp"
#include <unistd.h>
#include <memory>


using namespace ThreadSpace;


int main()
{
	// 用一个只能指针保证线程池的安全
	std::unique_ptr<ThreadPool<Task>> tp(new ThreadPool<Task>());

	tp->run();

    int x, y;
    char op;
    while(1)
    {
        std::cout << "请输入数据1# ";
        std::cin >> x;
        std::cout << "请输入数据2# ";
        std::cin >> y;
        std::cout << "请输入你要进行的运算#";
        std::cin >> op;
        Task t(x, y, op, mymath);
		tp->push(t);
		sleep(1);
	}
	return 0;
}