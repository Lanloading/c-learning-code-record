#define _CRT_SECURE_NO_WARNINGS
#include <iostream>
#include<string>

using namespace std;

struct ListNode
{

	struct ListNode* _next;
	int _val;

	ListNode(int val = 0)
		:_next(nullptr)
		,_val(val)
	{}
};


int main()
{
	//int* p1 = new int[10]{0,1,2,3,4};
	//delete[] p1;

	//ListNode* n1 = new ListNode(1);
	//ListNode* n2 = new ListNode(2);
	//ListNode* n3 = new ListNode(3);
	//ListNode* n4 = new ListNode(4);

	//n1->_next = n2;
	//n2->_next = n3;
	//n3->_next = n4;
	//n4->_next = nullptr;

	string st1;
	string st2("this is string\n");
	string st3(st2);
	string st4("it only got half of this centence\n", 10);
	string st5("1234");

	//cout << st1 << endl;
	//cout << st2 << endl;
	//cout << st3 << endl;
	//cout << st4 << endl;

	//char& operator[](size_t i)
	//{
	//	assert(i<_size);
	//	retrun _str[i];
	//}
	//

	//利用string内部封装好的成员函数遍历
	for (size_t i = 0; i < st5.size(); ++i)
	{
		st5[i]++;
	}
	cout << st5 << endl;



	//迭代器 类似指针的用法
	string::iterator its1 = st1.begin();
	while (its1 != st1.end())
	{
		*its1 += 1;
		its1++;
	}




	return 0;
}

//今天学习了更多的string接口，10.31日
string.push_back(' ');
string+=;
string.append();