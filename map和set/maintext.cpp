#define _CRT_SECURE_NO_WARNINGS
#include<iostream>
using namespace std;

#include"AVLTree.h"
#include"RBTree.h"

int main()
{
	cout << "RBTree" << endl;
	TestRBTree2();
	cout << "===================" << endl;
	AVL::TestAVLTree();
	return 0;
}