#pragma once




enum Color
{
	RED,
	BLACK,
};

template<class K,class V>
struct RBTreeNode
{
	pair<K, V> _kv;
	RBTreeNode<K,V>* _parent;
	RBTreeNode<K,V>* _left;
	RBTreeNode<K,V>* _right;
	Color _col;

	RBTreeNode(const pair<K, V>& kv)
		:_kv(kv)
		, _parent(nullptr)
		, _left(nullptr)
		, _right(nullptr)
		, _col(RED)
	{}
	

};

template<class K, class V>
class RBTree
{
	typedef RBTreeNode<K, V> Node;

public:
	bool Insert(const pair<K, V> kv)
	{
		//如果是空树，先建根
		if (_root == nullptr)
		{
			_root = new Node(kv);
			_root->_col = BLACK;
			return true;
		}

		//以正常的二叉搜索树的逻辑进行插入节点。
		Node* parent = nullptr;
		Node* cur = _root;

		while (cur)
		{
			if (kv.first > cur->_kv.first)
			{
				parent = cur;
				cur = cur->_right;
			}
			else if (kv.first < cur->_kv.first)
			{
				parent = cur;
				cur = cur->_left;
			}
			else
			{
				return false;
			}
		}

		cur = new Node(kv);
		cur->_col = RED;

		if (parent->_kv.first < cur->_kv.first)
		{
			parent->_right = cur;
			cur->_parent = parent;
		}
		else
		{
			parent->_left = cur;
			cur->_parent = parent;
		}
		//新节点链接后，需要以红黑树的规则调整树
		//什么时候才要调整？当出现两个相同的红节点的时候

		while (parent && parent->_col == RED)
		{
			//需要三个节点，叔叔，祖父，父亲
			Node* grandfather = parent->_parent;

			//先处理添加在祖父左树的情况
			if (parent == grandfather->_left)
			{
				Node* uncle = grandfather->_right;
				//情况1：叔叔存在且为红，则直接把父亲和叔叔变黑即可,祖父变红，然后继续向上查找。·

				if (uncle && uncle->_col == RED)
				{
					parent->_col = BLACK;
					uncle->_col = BLACK;
					grandfather->_col = RED;

					cur = grandfather;
					parent = cur->_parent;

				}
				else//情况2，3都是叔叔是黑,不用else if
				{
					//情况2：叔叔存在且为黑，将父亲节点右旋，父亲变黑，祖父变红,而且cur在parent的左树
					if (cur == parent->_left)
					{
						RotateR(grandfather);
						parent->_col = BLACK;
						grandfather->_col = RED;
					}
					else//情况3，cur在parent的右树,先左旋父亲节点，再右旋祖父节点,旋转完毕后，即情况2，但是
					{
						RotateL(parent);
						RotateR(grandfather);

						//此时的cur和parent置换了
						cur->_col = BLACK;
						grandfather->_col = RED;
					}

					break;
				}


			}
			else//新增节点在祖父右树的情况
			{
				Node* uncle = grandfather->_left;
				//还是情况1：叔叔存在且为红，父亲和叔叔变黑，祖父变红
				if (uncle && uncle->_col == RED)
				{
					parent->_col = uncle->_col = BLACK;
					grandfather->_col = RED;

					//向上继续
					cur = grandfather;
					parent = cur->_parent;
				}
				else//叔叔为黑
				{
					//情况2：叔叔存在且为黑,插入位置为右树,左旋转祖父节点，父亲变黑，祖父变红
					if (cur == parent->_right)
					{
						RotateL(grandfather);

						parent->_col = BLACK;
						grandfather->_col = RED;
					}
					else//情况3：叔叔存在且为黑，插入位置为左树，先右旋父亲拆解折线，再左旋祖父
					{
						RotateR(parent);
						RotateL(grandfather);

						//此时cur与parent位置交换
						cur->_col = BLACK;
						grandfather->_col = RED;


					}
					break;
				}

			}
		}
		_root->_col = BLACK;

		return true;

	}
	void RotateL(Node* parent)
	{

		Node* pparent = parent->_parent;
		Node* cur = parent->_right;

		parent->_right = cur->_left;
		if (cur->_left)
			cur->_left->_parent = parent;

		cur->_left = parent;
		parent->_parent = cur;

		if (pparent == nullptr)
		{
			_root = cur;
			_root->_parent = nullptr;
		}
		else//不等于空，旋转了一个子树
		{
			//pparent的孩子发生变动，判断是左子树还是右子树
			//若原先变动前的parent是pprant的左树
			if (pparent->_left == parent)
				pparent->_left = cur;
			else
				pparent->_right = cur;

			cur->_parent = pparent;

		}

	}
	void RotateR(Node* parent)
	{
		Node* pparent = parent->_parent;
		Node* cur = parent->_left;

		parent->_left = cur->_right;
		//如果cur的左子树不等于空，才链接过去
		if (cur->_right)
			cur->_right->_parent = parent;

		parent->_parent = cur;
		cur->_right = parent;


		if (pparent == nullptr)
		{
			_root = cur;
			_root->_parent = nullptr;
		}
		else
		{
			//pparent的孩子发生变动，判断是左子树还是右子树
			//若原先变动前的parent是pprant的左树
			if (pparent->_left == parent)
				pparent->_left = cur;
			else
				pparent->_right = cur;

			cur->_parent = pparent;

		}

	}

	int Getmark()
	{
		if (_root == nullptr)
			return 0;
		Node* cur = _root;

		int mark = 0;

		while (cur)
		{
			if (cur->_col == BLACK)
				++mark;

			cur = cur->_left;
		}

		return mark;
	}



	void Inorder()
	{
		_Inorder(_root,0,Getmark());
	}

	void _Inorder(Node* root,int black_num,int mark)
	{
		if (root == nullptr)
		{
			if (black_num != mark)
				cout << "违反规则:某条路径的黑色节点与最左路径不匹配" << endl;


			return;

		}

		//如果一个红色的节点，它的父亲不是黑的，那就违法规则。
		if (root->_col == RED && root->_parent->_col == RED)
		{

			cout << "违反规则：有两个相邻的红节点" << endl;
			return;
		}

		//所有路径上经过的黑节点数量是相同的，可以先遍历一条路径，获取一次黑节点的参考值，然后传参检查。
		if (root->_col == BLACK)

			++black_num;




		_Inorder(root->_left,black_num,mark);
		cout << root->_kv.first << ":" << root->_kv.second << endl;
		_Inorder(root->_right,black_num, mark);
	}


	bool Check(Node* root, int blackNum, const int ref)
	{
		if (root == nullptr)
		{
			//cout << blackNum << endl;
			if (blackNum != ref)
			{
				cout << "违反规则：本条路径的黑色节点的数量跟最左路径不相等" << endl;
				return false;
			}

			return true;
		}

		if (root->_col == RED && root->_parent->_col == RED)
		{
			cout << "违反规则：出现连续红色节点" << endl;
			return false;
		}

		if (root->_col == BLACK)
		{
			++blackNum;
		}

		return Check(root->_left, blackNum, ref)
			&& Check(root->_right, blackNum, ref);
	}

	bool IsBalance()
	{
		if (_root == nullptr)
		{
			return true;
		}

		if (_root->_col != BLACK)
		{
			return false;
		}

		int ref = 0;
		Node* left = _root;
		while (left)
		{
			if (left->_col == BLACK)
			{
				++ref;
			}

			left = left->_left;
		}

		return Check(_root, 0, ref);
	}

private:
	Node* _root = nullptr;
};

void TestRBTree1()
{
	//int a[] = { 8, 3, 1, 10, 6, 4, 7, 14, 13 };
	//int a[] = { 16, 3, 7, 11, 9, 26, 18, 14, 15 };
	int a[] = { 4, 2, 6, 1, 3, 5, 15, 7, 16, 14 };
	RBTree<int, int> t;
	for (auto e : a)
	{
		t.Insert(make_pair(e, e));
	}


	t.Inorder();

	cout << t.IsBalance() << endl;
}


void TestRBTree2()
{
	srand(time(0));
	const size_t N = 10000;
	RBTree<int, int> t;
	for (size_t i = 0; i < N; ++i)
	{
		size_t x = rand();
		t.Insert(make_pair(x, x));
	}

	t.Inorder();

	cout << t.IsBalance() << endl;
}
