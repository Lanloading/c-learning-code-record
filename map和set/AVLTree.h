#pragma once
#include<assert.h>
#include<time.h>


namespace AVL
{
	//左节点，右节点，平衡因子，存储的类型
	template<class K,class V>
	struct AVLNode
	{
		pair<K, V> _kv;
		AVLNode<K,V>* _left;
		AVLNode<K,V>* _right;
		AVLNode<K, V>* _parent;
		int _bf;

		AVLNode(const pair<K,V> &kv)
			:_kv(kv),_left(nullptr), _right(nullptr), _parent(nullptr), _bf(0)
		{}
	}; 


	template<class K, class V>
	class AVLTree
	{
		typedef AVLNode<K, V> Node;
	public:
		bool Insert(const pair<K,V> &kv)
		{
			//如果树为空，则创建一个节点
			if (_root == nullptr)
			{
				_root = new Node(kv);
				return true;
			}

			// 如果不为空，则找寻插入的位置,以Key作为插入根据，对比Key的大小
			//先检查当前插入的值应该往哪去
			Node* cur = _root;
			Node* parent = nullptr;
			while (cur)
			{
				if (kv.first > cur->_kv.first)
				{
					parent = cur;
					cur = cur->_right;
				}
				else if (kv.first < cur->_kv.first)
				{
					parent = cur;
					cur = cur->_left;
				}
				else
				{
					//相等,插入失败
					return false;
				}
			}

			//找到对应位置，新建节点。
			cur = new Node (kv);

			if (parent->_kv.first < cur->_kv.first)
			{
				parent->_right = cur;
				cur->_parent = parent;
			}
			else
			{
				parent->_left = cur;
				cur->_parent = parent;
			}

			//新建完节点之后，要更新平衡因子
			//插入右树则+1，插入左树则-1，当父亲节点的平衡因子为0的时候停止，最多修正至根
			while (parent)
			{
				//判断平衡因子该加还是该减少
				if (cur == parent->_left)
					parent->_bf--;
				else
					parent->_bf++;



				//移动完毕，平衡因子会有三种情况。等于0，不动，等于1，说明有变动，向上移动继续调整，最坏情况直到根节点
				//父亲的平衡因子等于0，不必再更新，直接break
				if (parent->_bf == 0)
				{
					break;
				}
				//更新之后进入不平衡状态，需要向上移动，因为造成了变动，还需要向上更新节点的平衡因子
				else if (parent->_bf == 1 || parent->_bf == -1)
				{
					cur = parent;
					parent = parent->_parent;
				}
				else if(parent->_bf == 2 || parent->_bf == -2)
					//此时AVL树已经失衡，需要旋转来修正
				{
					//旋转分为左旋转和右旋转，当更改来自右树时，也就是parent的bf==2，其右数的bf==1时，左旋转
					//右树失衡，左旋转
					if (parent->_bf == 2 && cur->_bf == 1)
						RotateL(parent);
					//左树失衡,右旋转
					else if (parent->_bf == -2 && cur->_bf == -1)
						RotateR(parent);
					//假如以pParent为根的子树不平衡，即pParent的平衡因子为2或者 - 2，分以下情况考虑
					//	1. pParent的平衡因子为2，说明pParent的右子树高，设pParent的右子树的根为pSubR
					//	当pSubR的平衡因子为1时，执行左单旋
					//	当pSubR的平衡因子为 - 1时，执行右左双旋
					//	2. pParent的平衡因子为 - 2，说明pParent的左子树高，设pParent的左子树的根为pSubL
					//	当pSubL的平衡因子为 - 1是，执行右单旋
					//	当pSubL的平衡因子为1时，执行左右双旋
					//	旋转完成后，原pParent为根的子树个高度降低，已经平衡，不需要再向上更新。
					else if (parent->_bf == -2 && cur->_bf == 1)
					{
						RotateLR(parent);
					}
					else if (parent->_bf == 2 && cur->_bf == -1)
					{
						RotateRL(parent);
					}
					else
						assert(false);

					break;

				}
				else//如果出现超过2的平衡因子，说明AVL树已经严重失衡，直接断死，不需要在做处理，整棵树的逻辑肯定出了问题
				{
					assert(false);
				}
			}

			return true;

		}

		void RotateL(Node* parent)
		{
			//旋转
			//parent变量所指向的节点一定是旋转轴点
			//	parent->right = parent->_right->_left
			//	那么parent的右孩子的_left指向parent， 也就是
			//	parent->_right->left = parent
			//	但还是需要额外处理一个问题，假如这次旋转只是处理了一个子树时，parent的right还需要更换祖宗
			//	所以还要一个pparent
			Node* pparent = parent->_parent;
			Node* cur = parent->_right;

			parent->_right = cur->_left;
			//如果cur的左子树不等于空，才链接过去
			if (cur->_left)
				cur->_left->_parent = parent;

			cur->_left = parent;
			parent->_parent = cur;

			//等于空,旋转了根节点
			if (pparent == nullptr)
			{
				_root = cur;
				_root->_parent = nullptr;
			}
			else//不等于空，旋转了一个子树
			{
				//pparent的孩子发生变动，判断是左子树还是右子树
				//若原先变动前的parent是pprant的左树
				if (pparent->_left == parent)
					pparent->_left = cur;
				else
					pparent->_right = cur;

				cur->_parent = pparent;

			}

			//旋转完毕，还需要更新平衡因子
			parent->_bf = cur->_bf = 0;
		}
		void RotateR(Node* parent)
		{
			Node* pparent = parent->_parent;
			Node* cur = parent->_left;

			parent->_left = cur->_right;
			//如果cur的左子树不等于空，才链接过去
			if (cur->_right)
				cur->_right->_parent = parent;

			parent->_parent = cur;
			cur->_right = parent;


			if (pparent == nullptr)
			{
				_root  = cur;
				_root->_parent = nullptr;
			}
			else
			{
				//pparent的孩子发生变动，判断是左子树还是右子树
				//若原先变动前的parent是pprant的左树
				if (pparent->_left == parent)
					pparent->_left = cur;
				else
					pparent->_right = cur;

				cur->_parent = pparent;

			}

			//旋转完毕，还需要更新平衡因子
			parent->_bf = cur->_bf = 0;

		}
		void RotateLR(Node* parent)
		{
			Node* subL = parent->_left;
			Node* subLR = subL->_right;

			int bf = subLR->_bf;

			RotateL(parent->_left);
			RotateR(parent);
			//左右双旋结束后，还需要更新正在这条折线上的平衡因子
			//平衡因子的更新情况有三种，因为造成左右双旋的原因是因为新增加的节点造成了折线式的失衡，才需要先左旋再右旋
			//那么针对折线底端的那个节点，既然是它造成了折线失衡，那么就需要处理当前节点的三种失衡情况，
			//1.当前节点增加在了它的右树，使其平衡因子+1
			//2.当前平衡因子增加在了它的左树，使其平衡因子-1
			//3.当前新增的节点就是其本身，平衡因子为0
			if (bf == -1) // subLR左子树新增
			{
				subL->_bf = 0;
				parent->_bf = 1;
				subLR->_bf = 0;
			}
			else if (bf == 1) // subLR右子树新增
			{
				parent->_bf = 0;
				subL->_bf = -1;
				subLR->_bf = 0;
			}
			else if (bf == 0) // subLR自己就是新增
			{
				parent->_bf = 0;
				subL->_bf = 0;
				subLR->_bf = 0;
			}
			else
			{
				assert(false);
			}

		}

		void RotateRL(Node* parent)
		{
			Node* subR = parent->_right;
			Node* subRL = subR->_left;

			int bf = subRL->_bf;


			RotateR(parent->_right);
			RotateL(parent);

			if (bf == -1) // subLR左子树新增
			{
				subR->_bf = 1;
				parent->_bf = 0;
				subRL->_bf = 0;
			}
			else if (bf == 1) // subLR右子树新增
			{
				parent->_bf = -1;
				subR->_bf = 0;
				subRL->_bf = 0;
			}
			else if (bf == 0) // subLR自己就是新增
			{
				parent->_bf = 0;
				subR->_bf = 0;
				subRL->_bf = 0;
			}
			else
			{
				assert(false);
			}
		}

		int Height(Node* root)
		{
			if (root == nullptr)
				return 0;

			int left = Height(root->_left) + 1;
			int right = Height(root->_right) + 1;

			return  max(left,right);
		}
		//int Height(Node* root)
		//{
		//	if (root == nullptr)
		//		return 0;

		//	int lh = Height(root->_left);
		//	int rh = Height(root->_right);

		//	return lh > rh ? lh + 1 : rh + 1;
		//}


		bool IsBalance()
		{
			return IsBalance(_root);
		}

		bool IsBalance(Node* root)
		{
			if (root == nullptr)
				return true;
			int leftHeight = Height(root->_left);
			int rightHeight = Height(root->_right);


			if (rightHeight - leftHeight != root->_bf)
			{
				cout << root->_kv.first << "平衡因子异常" << endl;
				return false;
			}

			return abs(rightHeight - leftHeight) < 2
				&& IsBalance(root->_left)
				&& IsBalance(root->_right);
		}

		void Inorder()
		{
			_Inorder(_root);
		}

		void _Inorder(Node* root)
		{
			if (root == nullptr)
				return;

			_Inorder(root->_left);
			cout << root->_kv.first << ":" << root->_kv.second << endl;
			_Inorder(root->_right);
		}

	private:
		Node* _root = nullptr;
	};


	void TestAVLTree()
{
	//int a[] = { 8, 3, 1, 10, 6, 4, 7, 14, 13 };
	//int a[] = { 16, 3, 7, 11, 9, 26, 18, 14, 15 };
	int a[] = { 4, 2, 6, 1, 3, 5, 15, 7, 16, 14 };
	AVLTree<int, int> t;
	for (auto e : a)
	{
		t.Insert(make_pair(e, e));
	}
	cout << t.IsBalance() << endl;


	t.Inorder();

	cout << t.IsBalance() << endl;
}



	void TestAVLTree2()
	{
		srand(time(0));
		const size_t N = 10000;
		AVLTree<int, int> t;
		for (size_t i = 0; i < N; ++i)
		{
			size_t x = rand();
			t.Insert(make_pair(x, x));
			//cout << t.IsBalance() << endl;
		}

		//t.Inorder();

		cout << t.IsBalance() << endl;
	}


}