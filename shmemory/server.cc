#include"common.hpp"

int main()
{
	//服务器端，创建和管理共享内存
	//创建前，先拿key
	key_t key = Getkey(PATH_NAME,PROJ_ID);

	//拿完key，创建共享内存
	int shmid = CreatShm(key);

	//服务器与共享内存关联，然后准备接收数据
	//接收的数据类型当作字符串处理
	char* data = (char*)AttachShm(shmid);
	//将void*强转成字符型之后，就相当于以字符为类型读取数据
	//由于服务端需要不断的读取数据，所以使用一个死循环
	while(true)
	{
		printf("从共享内存里捞数据：%s \n",data);
		sleep(1);
	}



	DetachShm((void*)data);

	return 0;
}