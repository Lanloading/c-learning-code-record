#include"common.hpp"

int main()
{
	//获取前，先拿key
	key_t key = Getkey(PATH_NAME,PROJ_ID);
	std::cout<<"已获得KEY！-》"<<key<<std::endl;

	//拿到key之后，获取对应的共享内存
	int shmid = GetShm(key);
	std::cout<<"对应内存ID-》"<<shmid<<std::endl;


	//得到起始地址后，关联内存
	char* push = (char* )AttachShm(shmid);
	int count = 1;
	while(true)
	{
		snprintf(push,MEM_SIZE,"当前扔进共享内存的消息为%d号消息",count++);
		sleep(3);
	}


	DetachShm((void*)push);


	return 0;
}