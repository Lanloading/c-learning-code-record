#pragma once
#include<iostream>
#include<vector>
#include<assert.h>

using namespace std;
namespace myvector
{
	template<class T>
	class vector
	{
	public:
		typedef T* iterator;
		typedef const T* const_iterator;

		//无参构造函数
		vector()
			:_start(nullptr), _size(nullptr), _capacity(nullptr)
		{}

		vector(int n, const T& val = T())
			:_start(nullptr)
			, _size(nullptr)
			, _capacity(nullptr)
		{
			reserve(n);
			for (int i = 0; i < n; ++i)
			{
				push_back(val);
			}
		}
		vector(size_t n, const T& val = T())
			:_start(nullptr)
			, _size(nullptr)
			, _capacity(nullptr)
		{
			reserve(n);
			for (size_t i = 0; i < n; ++i)
			{
				push_back(val);
			}
		}


		//析构函数
		~vector()
		{
			delete[] _start;
			_start = _size = _capacity = nullptr;
		}
		 
		vector<T>& operator=(vector<T> v)
		{
			swap(v);
			return *this;
		}


		T& operator[](size_t n)
		{
			assert(n < size());
			return *(_start+n);
		}
		//迭代器区间构造
		template<class Itreator>
		vector(Itreator begin, Itreator end) 
			:_start(nullptr), _size(nullptr), _capacity(nullptr)
		{
			while (begin != end)
			{
				push_back(*begin);
				++begin;
			}
		}

		//拷贝构造
		vector( const vector<T>& v)
			:_start(nullptr), _size(nullptr), _capacity(nullptr)
		{
			vector<T> tmp(v.begin(), v.end());
			swap(tmp);
		}


		void swap(vector<T>& v)
		{
			std::swap(_start, v._start);
			std::swap(_size, v._size);
			std::swap(_capacity, v._capacity);
		}

		size_t size()
		{
			return _size - _start;
		} 

		size_t size() const 
		{
			return _size - _start;
		}
		size_t capacity()
		{
			return _capacity - _start;
		} 

		size_t capacity() const
		{
			return _capacity - _start;
		}

		//迭代器
		iterator begin() 
		{
			return _start;
		}

		const_iterator begin() const
		{
			return _start;
		}

		iterator end() 
		{
			return _size ;
		}
		const_iterator end() const
		{
			return _size;
		}

		//能应对自定义类型的reserve
		void reserve(const size_t n)
		{
			if (n > capacity())
			{
				T* tmp = new T[n];

				size_t oldsize = size();

				if (_start)
				{
					for (size_t i = 0; i < oldsize; ++i)
					{
						tmp[i] = _start[i];
					}
					delete[]_start;
				}



				_start =  tmp;
				_size  =  tmp + oldsize;
				_capacity = _start + n;
			}
		}


		//不能应对的reserve
		//void reserve(const size_t n)
		//{
		//	if (n > capacity())
		//	{
		//		T* tmp = new T[n];

		//		size_t oldsize = size();

		//		if (_start != nullptr)
		//		{
		//			memcpy(tmp, _start, sizeof(T) * size());
		//			delete[] _start;
		//		}

		//		_start = tmp;
		//		_size = _start + oldsize;
		//		_capacity = _start + n;
		//	}
		//}

		void resize(const size_t n,T val = T())
		{
			if (n > capacity())
			{
				reserve(n);
			}

			if (n > size())
			{
				while (_size < _start + n)
				{
					*_size = val;
					++_size;
				}
			}
			else
			{
				_size = _start + n;
			}
		}

        ////
		void push_back(const T& val)
		{
			if (_size == _capacity)
			{
				size_t newcapacity = capacity() == 0 ? 4 : capacity() * 2;
				reserve(newcapacity);
			}

			*_size = val;
			++_size;
		}

		void pop_back()
		{
			assert(_size  > _start);

			--_size;
		}
		void insert(iterator n, const T& val)
		{
			assert(n >= _start);

			//扩容会引发迭代器失效的问题，需要更新迭代器
			if (_size == _capacity)
			{
				size_t length = n - _start;
				size_t newcapacity = capacity() == 0 ? 4 : capacity() * 2;
				reserve(newcapacity);
				n = _start + length;
			}

			if (n <  _size)
			{
				iterator end = _size;
				while (end > n)
				{
					*(end) = *(end - 1);
					end--;
				}

				*n = val;
				//整体大小+1，更新_size
				_size++;
			}
		}

		void erase(iterator n)
		{
			assert(n >= _start);
			assert(n < _size);
			
			if (n == _size - 1)
			{
				pop_back();
				return;
			}

			iterator cur = n + 1;
			while (cur < _size)
			{
				*(cur - 1) = *cur;
				++cur;
			}
			--_size;
		}


	private:
		iterator _start;
		iterator _size;
		iterator _capacity;
	};


	void text1()
	{
		vector<int> v1;
		v1.push_back(1);
		v1.push_back(2);
		v1.push_back(3);
		v1.push_back(4);
		v1.push_back(5);

	}

	void text2()
	{
		vector<int> v1;

		v1.push_back(1);

		cout << v1[0];
	}

	void text3()
	{
		vector<int> v1;
		v1.resize(10, -2);

		for (size_t i = 0; i < v1.size(); ++i)
		{
			cout << v1[i] << " ";
		}
		cout << endl;
		
		v1.resize(5, -2);

		for (size_t i = 0; i < v1.size(); ++i)
		{
			cout << v1[i] << " ";
		}

	}

	void text4()
	{
		vector<int> v1;
		v1.push_back(1);
		v1.push_back(2);
		v1.push_back(3);
		v1.push_back(4);

		v1.insert(v1.begin(), 5);

		for (size_t i = 0; i < v1.size(); ++i)
		{
			cout << v1[i] << " ";
		}

		v1.pop_back();
		v1.pop_back();
		cout << endl;
		for (size_t i = 0; i < v1.size(); ++i)
		{
			cout << v1[i] << " ";
		}

	}

	void text5()
	{
		vector<int> v1;
		v1.push_back(1);
		v1.push_back(2);
		v1.push_back(3);
		v1.push_back(4);
		v1.push_back(5);

		for (size_t i = 0; i < v1.size(); ++i)
		{
			cout << v1[i] << " ";
		}
		cout << endl;

		vector <int>::iterator pos = find(v1.begin(), v1.end(), 3);

		v1.erase(pos);
		for (size_t i = 0; i < v1.size(); ++i)
		{
			cout << v1[i] << " ";
		}
	}

	void text6()
	{
		vector<int> v1;
		v1.push_back(1);
		v1.push_back(2);
		v1.push_back(3);
		v1.push_back(4);
		v1.push_back(5);

		vector<int> v2(v1);

		for (auto& e : v2)
		{
			cout << e;
		}
	}

	void text7()
	{
		vector<vector<int>> vv1;
		vector<int> v1(10,5);
		vv1.push_back(v1);
		vv1.push_back(v1);
		vv1.push_back(v1);
		vv1.push_back(v1);
		vv1.push_back(v1);

		for (size_t i = 0; i < vv1.size(); ++i)
		{
			for (size_t j = 0; j < vv1[i].size(); ++j)
			{
				cout << vv1[i][j];

			}
			cout << endl;
		}

	}

}